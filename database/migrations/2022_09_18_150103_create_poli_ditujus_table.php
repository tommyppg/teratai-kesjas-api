<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('poli_ditujus', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('id_rikkes');
            $table->bigInteger('id_tindakan');
            $table->bigInteger('id_jenis_poli');
            $table->enum('status', ['undone', 'done'])->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('poli_ditujus');
    }
};
