<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('nama');
            $table->string('email')->unique()->nullable();
            $table->string('password')->nullable();
            $table->enum('role', ['superadmin', 'fktp', 'rs', 'rs_poli', 'anggota']);
            $table->string('nrp')->unique()->nullable();
            $table->string('pangkat')->nullable();
            $table->string('jabatan')->nullable();
            $table->string('satker')->nullable();
            $table->bigInteger('id_satker')->nullable();
            $table->string('jenis_kelamin')->nullable();
            $table->string('agama')->nullable();
            $table->string('gol_darah')->nullable();
            $table->string('alamat')->nullable();
            $table->string('no_hp')->nullable();
            $table->string('satuan_fungsi')->nullable();
            $table->timestamp('email_verified_at')->nullable();
            $table->enum('profile_status', ['complete', 'uncomplete'])->default('uncomplete');
            $table->enum('password_status', ['complete', 'uncomplete'])->default('uncomplete');
            $table->enum('status', ['active', 'inactive']);
            $table->rememberToken();
            $table->timestamps();

            // $table->foreign('id_satker')->references('id')->on('satkers');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
};
