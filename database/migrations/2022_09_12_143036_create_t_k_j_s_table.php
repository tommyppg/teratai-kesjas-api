<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tkj', function (Blueprint $table) {
            $table->id();
            $table->string('nrp')->nullable();
            $table->year('tahun_tkj')->nullable();
            $table->integer('bb')->nullable();
            $table->integer('tb')->nullable();
            $table->string('gol')->nullable();
            $table->string('samapta_a_hasil')->nullable();
            $table->string('samapta_a_nilai')->nullable();
            $table->string('samapta_b_pull_up_hasil')->nullable();
            $table->string('samapta_b_pull_up_nilai')->nullable();
            $table->string('samapta_b_sit_up_hasil')->nullable();
            $table->string('samapta_b_sit_up_nilai')->nullable();
            $table->string('samapta_b_push_up_hasil')->nullable();
            $table->string('samapta_b_push_up_nilai')->nullable();
            $table->string('samapta_b_shuttle_run_hasil')->nullable();
            $table->string('samapta_b_shuttle_run_nilai')->nullable();
            $table->string('nilai_rata_rata_b')->nullable();
            $table->string('nilai_akhir')->nullable();
            $table->string('kategori')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tkj');
    }
};
