<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rikkes', function (Blueprint $table) {
            $table->id();
            $table->string('nrp')->nullable();
            $table->enum('intensif', ['INTENSIF 1', 'INTENSIF 2', 'INTENSIF 3']);
            $table->year('tahun_rikkes')->nullable();
            $table->double('tb')->nullable();
            $table->double('bb')->nullable();
            $table->integer('sist')->nullable();
            $table->integer('dias')->nullable();
            $table->integer('nadi')->nullable();
            $table->text('kelainan')->nullable();
            $table->string('kualitas')->nullable();
            $table->integer('kuantitas')->nullable();
            $table->enum('status', ['locked', 'unlocked']);
            $table->timestamps();
            
            $table->foreign('nrp')->references('nrp')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rikkes');
    }
};
