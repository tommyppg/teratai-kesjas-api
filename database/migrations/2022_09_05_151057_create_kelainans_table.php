<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('kelainans', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('id_rikkes');
            $table->text('deskripsi');
            $table->bigInteger('id_daftar_kelainan')->nullable();
            $table->timestamps();

            // $table->foreign('id_rikkes')->references('id')->on('rikkes');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('kelainans');
    }
};
