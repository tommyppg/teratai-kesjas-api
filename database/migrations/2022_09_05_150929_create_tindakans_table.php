<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tindakans', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('id_rikkes');
            $table->bigInteger('id_satker_faskes')->nullable();
            $table->string('faskes_pribadi')->nullable();
            $table->text('tindakan');
            $table->enum('tipe', ['faskes', 'pribadi']);
            $table->enum('jenis', ['tindakan_akhir', 'butuh_rujukan']);
            $table->bigInteger('id_jenis_poli')->nullable();
            $table->string('nomor_pendaftaran')->nullable();
            $table->string('foto_resep')->nullable();
            $table->string('foto_obat')->nullable();
            $table->string('foto_kwitansi')->nullable();
            $table->enum('status', ['verified', 'unverified']);
            $table->dateTime('verified_date')->nullable();
            $table->softDeletes();
            $table->timestamps();

            // $table->foreign('id_rikkes')->references('id')->on('rikkes');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tindakans');
    }
};
