<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'nama' => "Super Administrator",
            'email' => "superadmin@terataikesjas.com",
            'password' => bcrypt("123456"),
            'role' => "superadmin",
            'status' => 'active'
        ]);

        DB::table('users')->insert([
            'nama' => "FKTP",
            'email' => "fktp@terataikesjas.com",
            'id_satker' => 7,
            'password' => bcrypt("123456"),
            'role' => "fktp",
            'status' => 'active'
        ]);

        DB::table('users')->insert([
            'nama' => "RS",
            'email' => "rs@terataikesjas.com",
            'id_satker' => 1,
            'password' => bcrypt("123456"),
            'role' => "rs",
            'status' => 'active'
        ]);

        DB::table('users')->insert([
            'nama' => "Poli Penyakit Dalam",
            'email' => "poli_penyakit_dalam@terataikesjas.com",
            'id_satker' => 2,
            'password' => bcrypt("123456"),
            'role' => "rs_poli",
            'status' => 'active'
        ]);

        DB::table('users')->insert([
            'nama' => "Poli Penyakit Jantung",
            'email' => "poli_penyakit_jantung@terataikesjas.com",
            'id_satker' => 3,
            'password' => bcrypt("123456"),
            'role' => "rs_poli",
            'status' => 'active'
        ]);

        DB::table('users')->insert([
            'nama' => "Poli Penyakit Mata",
            'email' => "poli_penyakit_mata@terataikesjas.com",
            'id_satker' => 4,
            'password' => bcrypt("123456"),
            'role' => "rs_poli",
            'status' => 'active'
        ]);

        DB::table('users')->insert([
            'nama' => "Poli Penyakit Orthopedi",
            'email' => "poli_penyakit_orthopedi@terataikesjas.com",
            'id_satker' => 5,
            'password' => bcrypt("123456"),
            'role' => "rs_poli",
            'status' => 'active'
        ]);

        DB::table('users')->insert([
            'nama' => "Poli Penyakit Paru",
            'email' => "poli_penyakit_paru@terataikesjas.com",
            'id_satker' => 6,
            'password' => bcrypt("123456"),
            'role' => "rs_poli",
            'status' => 'active'
        ]);
        
        // DB::table('users')->insert([
        //     'nama' => "A. BADRU TAMAMI",
        //     'email' => "badru@localhost.com",
        //     'password' => bcrypt("123456"),
        //     'role' => "anggota",
        //     'nrp' => "94060047",
        //     'pangkat' => "BHARAKA",
        //     'satker' => "RESIMEN III PAS PELOPOR",
        //     'jenis_kelamin' => "P",
        //     'profile_status' => 'uncomplete',
        //     'password_status' => 'uncomplete',
        //     'status' => 'active'
        // ]);
    }
}
