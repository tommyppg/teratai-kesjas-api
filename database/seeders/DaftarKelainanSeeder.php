<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DaftarKelainanSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $daftarKelainan = array(
            array(1,'Sedimen eritrosit pria'),
            array(1,'Sedimen silinder positif'),
            array(1,'Kolesterol'),
            array(1,'leukosit Neutrofil'),
            array(1,'Asam urat Pria'),
            array(1,'PLT'),
            array(1,'HIV'),
            array(1,'Kreatinin pria'),
            array(1,'BB'),
            array(1,'OW'),
            array(1,'Darah Leukosit'),
            array(1,'Sedimen leukosit pria'),
            array(1,'Trigliserida'),
            array(1,'SGOT'),
            array(1,'Haemoglobin'),
            array(1,'ND'),
            array(1,'Gula darah puasa'),
            array(1,'Laju Endap Darah'),
            array(1,'Sedimen eritrosit wanita'),
            array(1,'SGPT'),
            array(1,'Trombosit'),
            array(1,'Imuno Serologi VDRL'),
            array(1,'Ureum'),
            array(1,'urine'),
            array(1,'Urine'),
            array(1,'Sedimen leukosit wanita'),
            array(1,'Kreatinin wanita'),
            array(1,'Asam urat Wanita'),
            array(1,'Imuno Serologi HBs Ag'),
            array(1,'Bilirubin'),
            array(1,'leukosit Monosit'),
            array(2,'Sinus bradikardi'),
            array(2,'TD'),
            array(2,'ventrikular'),
            array(2,'Sinus aritmia'),
            array(2,'CTR'),
            array(2,'jantung'),
            array(2,'Gangguan konduksi blok intra atrial'),
            array(2,'Infark lama dengan ejection fraction'),
            array(2,'Infark pada dinding antero septal'),
            array(2,'Infark pada dinding inferior'),
            array(2,'Sinus takikardi'),
            array(2,'Dekstrokardia situs solitus'),
            array(2,'Iskemia miokardial depresi'),
            array(2,'Iskemia miokardial gelombang T inversi dalam'),
            array(2,'Mediastinum'),
            array(2,'Dilatasi aorta'),
            array(2,'Mekanisme atrial kontraksi prematur atrial'),
            array(2,'Iskemia miokardial perubahan segmen ST dan gelombang T yang non spesifik'),
            array(2,'Atherosklerosis'),
            array(3,'Visus VOD'),
            array(4,'Skoliosis Sedang'),
            array(4,'Skoliosis ringan'),
            array(4,'skeleton'),
            array(5,'Bronkopneumonia'),
            array(5,'Bronkiektasis'),
            array(5,'Pleura dan diafragma'),
            array(5,'Setiap kelainan aktif pada pleura'),
            array(5,'Kistik fibrosis'),
            array(5,'TBC paru'),
            array(5,'paru'),
            array(5,'Penebalan pleura'),
            array(5,'Kelainan di pleura bekas efusi pleura yang sudah sembuh dan faal paru normal'),
            array(5,'Efusi pleura'),
            array(5,'Penebalan pleura dengan faal paru normal'),
            array(5,'Pneumotoraks')
        );

        foreach($daftarKelainan as $daftar){
            DB::table('daftar_kelainans')->insert([
                'id_kategori_penyakit' => $daftar[0],
                'nama_kelainan' => $daftar[1],
            ]);
        }
    }
}
