<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Database\Seeders\UserSeeder;
use Database\Seeders\SatkerSeeder;
use Database\Seeders\KategoriPenyakitSeeder;
use Database\Seeders\DaftarKelainanSeeder;
use Database\Seeders\JenisPoliSeeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UserSeeder::class);
        $this->call(SatkerSeeder::class);
        $this->call(KategoriPenyakitSeeder::class);
        $this->call(DaftarKelainanSeeder::class);
        $this->call(JenisPoliSeeder::class);
        // \App\Models\User::factory(10)->create();

        // \App\Models\User::factory()->create([
        //     'name' => 'Test User',
        //     'email' => 'test@example.com',
        // ]);
    }
}
