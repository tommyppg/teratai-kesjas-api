<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class KategoriPenyakitSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $kategoriPenyakit = array(
            'Penyakit Dalam',
            'Penyakit Jantung',
            'Penyakit Mata',
            'Penyakit Orthopedi',
            'Penyakit Paru'
        );

        foreach($kategoriPenyakit as $kategori){
            DB::table('kategori_penyakits')->insert([
                'nama_kategori_penyakit' => $kategori
            ]);
        }
    }
}
