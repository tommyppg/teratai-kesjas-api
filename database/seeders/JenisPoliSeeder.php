<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class JenisPoliSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $namaJenisPoli = array(
            'Penyakit Dalam',
            'Penyakit Jantung',
            'Penyakit Mata',
            'Penyakit Orthopedi',
            'Penyakit Paru'
        );

        foreach($namaJenisPoli as $jenisPoli){
            DB::table('jenis_polis')->insert([
                'nama_jenis_poli' => $jenisPoli
            ]);
        }
    }
}
