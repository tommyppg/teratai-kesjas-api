<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class SatkerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('satkers')->insert([
            'nama_satker' => "Rumah Sakit Bhayangkara Brimob",
            'tipe' => "rs"
        ]);

        DB::table('satkers')->insert([
            'nama_satker' => "Poli Penyakit Dalam",
            'tipe' => "rs_poli",
            'id_dibawah_koordinasi_satker' => 1,
            'id_jenis_poli' => 1
        ]);

        DB::table('satkers')->insert([
            'nama_satker' => "Poli Penyakit Jantung",
            'tipe' => "rs_poli",
            'id_dibawah_koordinasi_satker' => 1,
            'id_jenis_poli' => 2
        ]);

        DB::table('satkers')->insert([
            'nama_satker' => "Poli Penyakit Mata",
            'tipe' => "rs_poli",
            'id_dibawah_koordinasi_satker' => 1,
            'id_jenis_poli' => 3
        ]);

        DB::table('satkers')->insert([
            'nama_satker' => "Poli Penyakit Orthopedi",
            'tipe' => "rs_poli",
            'id_dibawah_koordinasi_satker' => 1,
            'id_jenis_poli' => 4
        ]);

        DB::table('satkers')->insert([
            'nama_satker' => "Poli Penyakit Paru",
            'tipe' => "rs_poli",
            'id_dibawah_koordinasi_satker' => 1,
            'id_jenis_poli' => 5
        ]);

        DB::table('satkers')->insert([
            'nama_satker' => "Klinik Sikesjas Korbrimob",
            'tipe' => "fktp"
        ]);
    }
}
