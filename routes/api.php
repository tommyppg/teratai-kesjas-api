<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\RikkesController;
use App\Http\Controllers\TKJController;
use App\Http\Controllers\TindakanController;
use App\Http\Controllers\NewsController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['prefix' => 'auth'], function ($router) {
    Route::post('/login', [AuthController::class, 'login']);
    Route::post('/register', [AuthController::class, 'register']);
    Route::post('/logout', [AuthController::class, 'logout'])->middleware('jwt.verify');
    Route::post('/refresh', [AuthController::class, 'refresh'])->middleware('jwt.verify');
    Route::get('/user-profile', [AuthController::class, 'userProfile'])->middleware('jwt.verify');
    Route::post('/change-password', [AuthController::class, 'changePassword']);
    Route::post('/reset-password', [AuthController::class, 'resetPassword']);
    Route::post('/update-user', [AuthController::class, 'updateUser'])->middleware('jwt.verify');
});

Route::group(['prefix' => 'rikkes'], function ($router) {
    Route::get('/get-all', [RikkesController::class, 'getAll'])->middleware('jwt.verify');
    Route::get('/get-by-id', [RikkesController::class, 'getById'])->middleware('jwt.verify');
    Route::get('/get-by-tahun', [RikkesController::class, 'getByTahun'])->middleware('jwt.verify');
});

Route::group(['prefix' => 'tindakan'], function ($router) {
    Route::get('/get-all', [TindakanController::class, 'getAll'])->middleware('jwt.verify');
    Route::get('/get-by-id', [TindakanController::class, 'getById'])->middleware('jwt.verify');
    Route::post('/insert', [TindakanController::class, 'insert'])->middleware('jwt.verify');
    Route::post('/update', [TindakanController::class, 'update'])->middleware('jwt.verify');
});

Route::group(['prefix' => 'tkj'], function ($router) {
    Route::get('/get-all', [TKJController::class, 'getAll'])->middleware('jwt.verify');
    Route::get('/get-by-id', [TKJController::class, 'getById'])->middleware('jwt.verify');
    Route::get('/get-by-tahun', [TKJController::class, 'getByTahun'])->middleware('jwt.verify');
});

Route::group(['prefix' => 'news'], function ($router) {
    Route::get('/get-all', [NewsController::class, 'getAll'])->middleware('jwt.verify');
    Route::get('/get-by-id', [NewsController::class, 'getById'])->middleware('jwt.verify');
});

Route::get('storage/{filename}', function ($filename)
{
    $path = storage_path('app/public/files/' . $filename);

    if (!File::exists($path)) {
        abort(404);
    }

    $file = File::get($path);
    $type = File::mimeType($path);

    $response = Response::make($file, 200);
    $response->header("Content-Type", $type);

    return $response;
});

Route::get('/clear_cache', function () {
    \Artisan::call('config:cache');
    \Artisan::call('view:clear');
    \Artisan::call('route:clear');
    dd("Cache is cleared");
});