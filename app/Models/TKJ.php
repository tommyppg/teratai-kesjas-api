<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TKJ extends Model
{
    use HasFactory;

    protected $table = "tkj";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'tahun_tkj',
        'nrp',
        'bb',
        'tb',
        'gol',
        'samapta_a_hasil',
        'samapta_a_nilai',
        'samapta_b_pull_up_hasil',
        'samapta_b_pull_up_nilai',
        'samapta_b_sit_up_hasil',
        'samapta_b_sit_up_nilai',
        'samapta_b_push_up_hasil',
        'samapta_b_push_up_nilai',
        'samapta_b_shuttle_run_hasil',
        'samapta_b_shuttle_run_nilai',
        'nilai_rata_rata_b',
        'nilai_akhir',
        'kategori',
        'created_at',
        'updated_at'
    ];
}
