<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

class Tindakan extends Model
{
    use HasFactory, SoftDeletes;

    public $timestamps = true;

    protected $fillable = [
        'id_rikkes',
        'id_satker_faskes',
        'faskes_pribadi',
        'tindakan',
        'tipe',
        'jenis',
        'id_jenis_poli',
        'nomor_pendaftaran',
        'foto_resep',
        'foto_obat',
        'foto_kwitansi',
        'status',
        'created_at',
        'updated_at'
    ];  

    protected $dates = ['deleted_at'];
}
