<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Rikkes extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'nrp',
        'tahun_rikkes',
        'intensif',
        'tb',
        'bb',
        'sist',
        'dias',
        'nadi',
        'kelainan',
        'kualitas',
        'kuantitas',
        'created_at',
        'updated_at'
    ];
}
