<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class NewsReader extends Model
{
    use HasFactory;

    protected $fillable = [
        'id_news',
        'id_user',
        'created_at',
        'updated_at'
    ];
}
