<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DaftarKelainan extends Model
{
    use HasFactory;

    protected $fillable = [
        'id_kategori_penyakit',
        'nama_kelainan',
        'created_at',
        'updated_at'
    ];
}
