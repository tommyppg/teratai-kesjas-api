<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\User;
use App\Models\TKJ;
use Validator;

class TKJController extends Controller
{
    public function getAll() {
        $nrp = auth()->user()->nrp;

        return response()->json(TKJ::where('nrp',$nrp)->get());
    }

    public function getById(Request $request) {
        $nrp = auth()->user()->nrp;
        $idTKJ = $request->id;

        return response()->json(TKJ::where(['nrp' => $nrp, 'id' => $idTKJ])->first());
    }

    public function getByTahun(Request $request) {
        $nrp = auth()->user()->nrp;
        $tahun_tkj = $request->tahun_tkj;

        return response()->json(TKJ::where(['nrp' => $nrp, 'tahun_tkj' => $tahun_tkj])->first());
    }
}