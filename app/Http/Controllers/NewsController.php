<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\User;
use App\Models\News;
use App\Models\NewsReader;
use Illuminate\Support\Facades\DB;
use Validator;
use Config;

class NewsController extends Controller
{
    public function getAll() {
        $nrp = auth()->user()->nrp;
        
        $allNews = News::select('news.*', 'users.nama as created_by_name')->join('users', 'users.id', '=', 'news.created_by')->where('news.status', 'published')->orderBy('created_at', 'desc')->get();

        foreach($allNews as $key => $news){
            if($news->lampiran != null){
                $allNews[$key]['lampiran_url'] = Config::get('app.app_url').'news_file/'.$news->lampiran;
            }else{
                $allNews[$key]['lampiran_url'] = null;
            }
            
            $newsReader = NewsReader::where(['id_user' => auth()->user()->id, 'id_news' => $news->id])->get();

            if(count($newsReader) > 0){
                $allNews[$key]['read_status'] = true;
            }else{
                $allNews[$key]['read_status'] = false;
            }
        }

        return response()->json($allNews);
    }

    public function getById(Request $request) {
        $nrp = auth()->user()->nrp;
        $idNews = $request->id;

        $news = News::select('news.*', 'users.nama as created_by_name')->join('users', 'users.id', '=', 'news.created_by')->where('news.id', $idNews)->first();

        if($news->lampiran != null){
            $news['lampiran_url'] = Config::get('app.app_url').'news_file/'.$news->lampiran;
        }else{
            $news['lampiran_url'] = null;
        }
        
        $newsReader['id_news'] = $idNews;
        $newsReader['id_user'] = auth()->user()->id;
        $insertNewsReader = NewsReader::create($newsReader);

        return response()->json($news);
    }
}