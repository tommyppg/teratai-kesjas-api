<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\User;
use App\Models\Rikkes;
use App\Models\KategoriPenyakit;
use App\Models\DaftarKelainan;
use App\Models\Kelainan;
use App\Models\PoliDituju;
use Illuminate\Support\Facades\DB;
use Validator;

class RikkesController extends Controller
{
    public function getAll() {
        $nrp = auth()->user()->nrp;
        
        $allRikkes = Rikkes::where('nrp',$nrp)->get();

        foreach($allRikkes as $key => $rikkes){
            $allRikkes[$key]->poli_tujuan = PoliDituju::select('jenis_polis.id as id_jenis_poli', 'nama_jenis_poli')
                            ->where('poli_ditujus.id_rikkes', $rikkes->id)
                            ->join('jenis_polis', 'jenis_polis.id', '=', 'poli_ditujus.id_jenis_poli')
                            ->orderBy('id_jenis_poli')
                            ->get();
        }

        return response()->json($allRikkes);
    }

    public function getById(Request $request) {
        $nrp = auth()->user()->nrp;
        $idRikkes = $request->id;

        $rikkes = (object)[];
        $rikkes = Rikkes::where(['nrp' => $nrp, 'id' => $idRikkes])->first();
        $rikkes->poli_tujuan = PoliDituju::select('jenis_polis.id as id_jenis_poli', 'nama_jenis_poli')
                            ->where('poli_ditujus.id_rikkes', $rikkes->id)
                            ->join('jenis_polis', 'jenis_polis.id', '=', 'poli_ditujus.id_jenis_poli')
                            ->orderBy('id_jenis_poli')
                            ->get();

        return response()->json($rikkes);
    }

    public function getByTahun(Request $request) {
        $nrp = auth()->user()->nrp;
        $tahun_rikkes = $request->tahun_rikkes;

        $rikkes = (object)[];
        $rikkes = Rikkes::where(['nrp' => $nrp, 'tahun_rikkes' => $tahun_rikkes])->first();
        $rikkes->poli_tujuan = PoliDituju::select('jenis_polis.id as id_jenis_poli', 'nama_jenis_poli')
                            ->where('poli_ditujus.id_rikkes', $rikkes->id)
                            ->join('jenis_polis', 'jenis_polis.id', '=', 'poli_ditujus.id_jenis_poli')
                            ->orderBy('id_jenis_poli')
                            ->get();

        return response()->json($rikkes);
    }
}