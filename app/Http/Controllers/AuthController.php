<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\DB;
use App\Models\User;
use Validator;
use Carbon\Carbon;
use App\Mail\TerataikesjasEmail; 

class AuthController extends Controller
{
    /**
     * Create a new AuthController instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('jwt.verify', ['except' => ['login', 'register', 'setPassword', 'resetPassword']]);
    }
    /**
     * Get a JWT via given credentials.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function login(Request $request){
    	$validator = Validator::make($request->all(), [
            'nrp' => 'required',
            'password' => 'required|string|min:6',
        ]);
        if ($validator->fails()) {
            return response()->json($validator->errors(), 422);
        }
        if (! $token = auth()->attempt(['nrp' => $request->nrp, 'password' => $request->password, 'status' => 'active'])) {
            return response()->json(['error' => 'Unauthorized'], 401);
        }
        return $this->createNewToken($token);
    }
    /**
     * Register a User.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function register(Request $request) {
        $validator = Validator::make($request->all(), [
            'nama' => 'required|string|between:2,100',
            'email' => 'required|string|email|max:100|unique:users',
            'password' => 'required|string|confirmed|min:6',
            'role' => 'required',
        ]);
        if($validator->fails()){
            return response()->json($validator->errors()->toJson(), 400);
        }
        $user = User::create(array_merge(
                    $validator->validated(),
                    ['password' => bcrypt($request->password)]
                ));
        return response()->json([
            'message' => 'User successfully registered',
            'user' => $user
        ], 201);
    }

    /**
     * Log the user out (Invalidate the token).
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout() {
        auth()->logout();
        return response()->json(['message' => 'User successfully signed out']);
    }
    /**
     * Refresh a token.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function refresh() {
        return $this->createNewToken(auth()->refresh());
    }
    /**
     * Get the authenticated User.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function userProfile() {
        return response()->json(auth()->user());
    }
    /**
     * Get the token array structure.
     *
     * @param  string $token
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function createNewToken($token){
        auth()->factory()->setTTL(10080);
        
        return response()->json([
            'access_token' => $token,
            'token_type' => 'bearer',
            'expires_in' => auth()->factory()->getTTL() * 60,
            'user' => auth()->user()
        ]);
    }

    public function changePassword(Request $request){
        $validator = Validator::make($request->all(), [
            'password' => 'required|string'
        ]);
        if($validator->fails()){
            return response()->json($validator->errors()->toJson(), 400);
        }

        $update = User::where('nrp', auth()->user()->nrp)->update(['password' => bcrypt($request->password), 'password_status' => 'complete']);

        if($update){
            return response()->json(['message' => 'Sukses mengupdate password']);
        }else{
            return response()->json(['message' => 'Gagal update. Mohon coba kembali.'], 400);
        }
    }
    
    public function resetPassword(Request $request){
        $validator = Validator::make($request->all(), [
            'email' => 'required'
        ]);

        if($validator->fails()){
            return response()->json($validator->errors()->toJson(), 400);
        }

        $user = DB::table('users')->where('email', $request->email)->first();

        if(!$user){
            return response()->json(['message' => 'Email tidak ditemukan!'], 400);
        }
        
        $token = $this->quickRandom(64);

        DB::table('password_resets')->insert(
            ['email' => $request->email, 'token' => $token, 'created_at' => Carbon::now()]
        );

        $subject = "Reset Password Anggota";
        $view = "resetpasswordanggota";
        $data['token'] = $token;
        $data['user'] = $user;

        try {
            Mail::to($request->email)->send(new TerataikesjasEmail($subject, $view, $data));
        } catch (Exception $e) {
            return response()->json(['message' => 'Gagal mengirim email password. Silakan coba lagi.'], 400);
        }
        

        return response()->json(['message' => 'Sukses mengirimkan email reset password.']);
    }

    private function quickRandom($length = 16)
    {
        $pool = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';

        return substr(str_shuffle(str_repeat($pool, 5)), 0, $length);
    }

    public function updateUser(Request $request){
        $validator = Validator::make($request->all(), [
            'email' => 'required',
            'no_hp' => 'required',
            'nama' => 'required',
            // 'jabatan' => 'required',
            // 'jenis_kelamin' => 'required',
            // 'agama' => 'required',
            // 'gol_darah' => 'required',
            // 'alamat' => 'required',
        ]);

        if($validator->fails()){
            return response()->json($validator->errors()->toJson(), 400);
        }

        $nrp = auth()->user()->nrp;

        $user['nama'] = $request->nama;
        $user['email'] = $request->email;
        $user['jabatan'] = $request->jabatan;
        $user['jenis_kelamin'] = $request->jenis_kelamin;
        $user['agama'] = $request->agama;
        $user['gol_darah'] = $request->gol_darah;
        $user['alamat'] = $request->alamat;
        $user['no_hp'] = $request->no_hp;
        $user['profile_status'] = 'complete';
        
        try { 
            $update = DB::table('users')->where('nrp', $nrp)->update($user);
        } catch(\Illuminate\Database\QueryException $ex){ 
            if($ex->getCode() == "23000"){
                return response()->json(['message' => 'Email sudah terdaftar di sistem.'], 400);
            }else{
                return response()->json(['message' => 'Gagal update data. Silakan coba lagi'], 400);
            }
        }
        
        if($update){
            return response()->json(['message' => 'Berhasil update data.']);
        }else{
            return response()->json(['message' => 'Gagal update data. Silakan coba lagi.'], 400);
        }
    }
}