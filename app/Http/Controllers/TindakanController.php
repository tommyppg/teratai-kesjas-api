<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\User;
use App\Models\Rikkes;
use App\Models\Tindakan;
use App\Models\PoliDituju;
use Validator;
use Illuminate\Support\Facades\DB;

class TindakanController extends Controller
{
    public function getAll(Request $request) {
        $id_rikkes = $request->id_rikkes;

        return response()->json(Tindakan::where('id_rikkes',$id_rikkes)->get());
    }

    public function getById(Request $request) {
        $id_tindakan = $request->id;

        $rikkes = (object)[];
        $tindakan = Tindakan::where(['id' => $id_tindakan])->first();

        if($tindakan->jenis == "butuh_rujukan"){
            $tindakan['poli_dituju'] = PoliDituju::select('poli_ditujus.id_jenis_poli', 'jenis_polis.nama_jenis_poli')->join('jenis_polis', 'jenis_polis.id', '=', 'poli_ditujus.id_jenis_poli')->where(['id_tindakan' => $tindakan->id])->get();
        }else{
            $tindakan['poli_dituju'] = null;
        }
        

        return response()->json($tindakan);
    }

    public function insert(Request $request){
        //validate form
        $validator = Validator::make($request->all(), [
            'id_rikkes' => 'required',
            'faskes_pribadi' => 'required',
            'tindakan' => 'required',
            'foto_resep' => 'mimes:pdf,jpg,jpeg,png',
            'foto_obat' => 'mimes:pdf,jpg,jpeg,png',
            'foto_kwitansi' => 'mimes:pdf,jpg,jpeg,png',
        ]);

        if($validator->fails()){
            return response()->json($validator->errors()->toJson(), 400);
        }

        $tindakan = Array();
        $tindakan['id_rikkes'] = $request->id_rikkes;
        $tindakan['faskes_pribadi'] = $request->faskes_pribadi;
        $tindakan['tindakan'] = $request->tindakan;
        $tindakan['tipe'] = 'pribadi';
        $tindakan['jenis'] = 'tindakan_akhir';
        $tindakan['id_jenis_poli'] = $request->id_jenis_poli;
        $tindakan['nomor_pendaftaran'] = $request->nomor_pendaftaran;
        $tindakan['status'] = 'unverified';

        if($request->has('foto_resep')){
            $path_foto_resep = $request->file('foto_resep')->store('public/files');
            $name_foto_resep = $request->file('foto_resep')->getClientOriginalName();
            
            $tindakan['foto_resep'] = pathinfo($path_foto_resep)['basename'];
        }

        if($request->has('foto_obat')){
            $path_foto_obat = $request->file('foto_obat')->store('public/files');
            $name_foto_obat = $request->file('foto_obat')->getClientOriginalName();

            $tindakan['foto_obat'] = pathinfo($path_foto_obat)['basename'];
        }

        if($request->has('foto_kwitansi')){
            $path_foto_kwitansi = $request->file('foto_kwitansi')->store('public/files');
            $name_foto_kwitansi = $request->file('foto_kwitansi')->getClientOriginalName();

            $tindakan['foto_kwitansi'] = pathinfo($path_foto_kwitansi)['basename'];
        }
        
        $insert = Tindakan::create($tindakan);

        if($insert){
            return response()->json(['message' => 'Sukses menyimpan']);
        }else{
            return response()->json(['message' => 'Gagal menyimpan. Mohon coba kembali.'], 400);
        }
    }

    public function update(Request $request){
        //validate form
        $validator = Validator::make($request->all(), [
            'id' => 'required',
            'faskes_pribadi' => 'required',
            'tindakan' => 'required',
        ]);

        if($validator->fails()){
            return response()->json($validator->errors()->toJson(), 400);
        }
        
        $tindakan = Array();
        $tindakan['faskes_pribadi'] = $request->faskes_pribadi;
        $tindakan['tindakan'] = $request->tindakan;
        $tindakan['tipe'] = 'pribadi';
        $tindakan['jenis'] = 'tindakan_akhir';
        $tindakan['id_jenis_poli'] = $request->id_jenis_poli;
        $tindakan['nomor_pendaftaran'] = $request->nomor_pendaftaran;
        $tindakan['status'] = 'unverified';

        if($request->has('foto_resep')){
            $path_foto_resep = $request->file('foto_resep')->store('public/files');
            $name_foto_resep = $request->file('foto_resep')->getClientOriginalName();
            
            $tindakan['foto_resep'] = pathinfo($path_foto_resep)['basename'];
        }

        if($request->has('foto_obat')){
            $path_foto_obat = $request->file('foto_obat')->store('public/files');
            $name_foto_obat = $request->file('foto_obat')->getClientOriginalName();

            $tindakan['foto_obat'] = pathinfo($path_foto_obat)['basename'];
        }

        if($request->has('foto_kwitansi')){
            $path_foto_kwitansi = $request->file('foto_kwitansi')->store('public/files');
            $name_foto_kwitansi = $request->file('foto_kwitansi')->getClientOriginalName();

            $tindakan['foto_kwitansi'] = pathinfo($path_foto_kwitansi)['basename'];
        }

        $update = Tindakan::where('id', $request->id)->update($tindakan);

        if($update){
            return response()->json(['message' => 'Sukses menyimpan']);
        }else{
            return response()->json(['message' => 'Gagal menyimpan. Mohon coba kembali.'], 400);
        }
    }
}