<?php
 
namespace App\Mail;
 
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
 
class TerataikesjasEmail extends Mailable
{
    use Queueable, SerializesModels;
    
    private $subject_name;
    private $view_name;
    private $data;
 
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($subject_name, $view_name, $data = null)
    {
        $this->subject_name = $subject_name;
        $this->view_name = $view_name;
        $this->data = $data;
    }
 
    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
       return $this->from('no.reply@terataikesjas.com')
                    ->subject($this->subject_name)
                    ->view('mail/'.$this->view_name)
                    ->with($this->data);
    }
}